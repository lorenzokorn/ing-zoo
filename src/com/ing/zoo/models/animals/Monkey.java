package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.Trickable;

import java.util.Random;

public class Monkey extends Omnivore implements Trickable {
    public String helloText;
    public String eatText;

    public Monkey(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "Did... someone say peanut butter?";
        System.out.println(helloText);
    }

    public void eatLeaves() {
        eatText = "Buh-blg-golp... uh... excuse me.";
        System.out.println(eatText);
    }

    public void eatMeat() {
        eatText = "*smak* *smak* ... ssjlp";
        System.out.println(eatText);
    }

    public void performTrick() {
        String[] tricks = new String[]{"monkey dance", "backflip"};
        Random random = new Random();
        int rnd = random.nextInt(tricks.length);
        System.out.println(tricks[rnd]);
    }
}
