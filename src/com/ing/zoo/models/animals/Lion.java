package com.ing.zoo.models.animals;

public class Lion extends Carnivore {
    public String helloText;
    public String eatText;

    public Lion(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "roooaoaaaaar";
        System.out.println(helloText);
    }

    public void eatMeat() {
        eatText = "nomnomnom thx mate";
        System.out.println(eatText);
    }
}
