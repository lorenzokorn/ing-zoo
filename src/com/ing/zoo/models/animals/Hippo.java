package com.ing.zoo.models.animals;

public class Hippo extends Herbivore {
    public String helloText;
    public String eatText;

    public Hippo(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "splash";
        System.out.println(helloText);
    }

    public void eatLeaves() {
        eatText = "munch munch lovely";
        System.out.println(eatText);
    }
}
