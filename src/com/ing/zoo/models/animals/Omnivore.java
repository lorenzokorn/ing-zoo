package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.LeafEater;
import com.ing.zoo.interfaces.MeatEater;

public class Omnivore extends Animal implements LeafEater, MeatEater {
    public Omnivore(String name) {
        super(name);
    }

    public void eatLeaves() {
        System.out.println("Nom nom");
    }

    public void eatMeat() {
        System.out.println("Nom nom");
    }
}
