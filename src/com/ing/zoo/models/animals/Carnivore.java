package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.MeatEater;

public class Carnivore extends Animal implements MeatEater {

    public Carnivore(String name) {
        super(name);
    }

    public void eatMeat() {
        System.out.println("Nom nom");
    }
}
