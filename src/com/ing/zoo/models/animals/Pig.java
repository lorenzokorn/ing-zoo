package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.Trickable;

import java.util.Random;

public class Pig extends Omnivore implements Trickable {
    public String helloText;
    public String eatText;

    public Pig(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "splash";
        System.out.println(helloText);
    }

    public void eatLeaves() {
        eatText = "munch munch oink";
        System.out.println(eatText);
    }

    public void eatMeat() {
        eatText = "nomnomnom oink thx";
        System.out.println(eatText);
    }

    public void performTrick() {
        String[] tricks = new String[]{
                "rolls in the mud",
                "runs in circles"
        };
        Random random = new Random();
        int rnd = random.nextInt(tricks.length);
        System.out.println(tricks[rnd]);
    }
}
